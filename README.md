# CSVLog - Log stuff to CSV

This Go library eases the management of CSV files.

## Install

	go get gitlab.inria.fr/aluxey/csvlog 

## Usage

	SetOutputLocation(path string) error

`CSVLog` outputs files to the current directory.
Using this function, you can change it to the parameter `path`.

	New(filename string, header []string) (*CSVLog, error)

Creates a CSV file called `filename` and puts the first line: the `header`.
Returns a `CSVLog` object, that will be used to add firther lines.

	(l *CSVLog) Write(line []string) error

Given an existing `CSVLog` object, adds the new `line` to the output file.
Obviously `len(line)` should equal `len(header)`.

----

By Adrien "Lazy-Ass" Luxey in June 2018.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.